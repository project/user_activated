Stores the time when users got activated.

This is mostly an API module. All it does by itself is storing the UNIX
timestamp when the user was activated (last time), and feeds that info back to
the $user object upon user_load(), and (with proper permissions) to the user
view (user/%user) and user edit (user/%user/edit) pages. Should you make use
of that info for anything else, have the {user_activated} table JOINed to your
query by its uid field.
